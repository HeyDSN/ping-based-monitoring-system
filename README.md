![alt text](https://d1q6f0aelx0por.cloudfront.net/product-logos/5431a80b-9ab9-486c-906a-e3d4b5ccaa96-hello-world.png "HOLA!")

Live preview is running on [I'm live here!](http://165.22.106.152)

# Ping Based Monitoring System

## Backend Developer Assessment

### Nama Project : Ping Based Monitoring System

### Waktu Pengumpulan : Due date, July 18th 2018 23.59 WIB

### Detil Teknis : Sesuai dokumen terlampir

# How to setup

# 1. Tanpa Docker

Untuk mejalankannya tanpa docker cukup dengan melakukan installasi depedencies modulenya, secara general caranya seperti konfigurasi aplikasi nodejs lainnya.
Berikut step-stepnya `(Dengan asumsi nodejs telah terinstall dan database MySQL tersedia untuk digunakan.)`:

1. Clone repo ini ke local
2. Setelah clone selesai, masuk ke dalam folder aplikasi ini
3. Install depedencies dengan perintah `npm install`
4. Siapkan database dan import scheme table `(file: table.sql)` yang telah disediakan pada folder `db` dalam folder aplikasi ini
5. Salin file `.env-example` ke `.env` dan edit file `.env` yang telah dibuat sesuai dengan konfigurasi yang dibutuhkan
6. Salin file `device-list.example.json` ke `device-list.json` dan edit file `device-list.json` yang telah dibuat sesuai dengan konfigurasi yang dibutuhkan, file ini berisikan daftar device yang akan di monitor, isikan `IP` dan `deviceName` pada file konfigurasi sebelum memulai aplikasi
7. Setelah seluruh konfigurasi selesai aplikasi dapat dijalankan dengan command `npm start`


---
# 2. Dengan Docker

Untuk menjalankannya melalui sistem dockerize cukup sesuaikan `docker-compose.yml` sesuai kebutuhakn. Template dapat dilihat pada file `docker-compose.example.yml`, silahkan salin file tersebut dan sesuaikan dengan kebutuhan.

```
NodeJS dan MySQL dibuild dalam 1 Docker agar mempermudah deployment.
```

---

```
Setelah beberapa menit data device availability akan dapat dilihat pada API url:
`[GET] {url}/device-availability`. Monitor akan berjalan setiap 5 detik untuk setiap device.
```

```
Monitoring ini dibuat dengan menggunakan nodejs dan telah di test menggunakan docker mampun tanpa docker pada environment Windows 10 Pro Build 1903.
```
