module.exports = (sequelize, DataTypes) => {
	const Log = sequelize.define('Log', {
		ID: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
			unique: true,
		},
		IP: DataTypes.STRING(255),
		deviceName: DataTypes.STRING(255),
		deviceStatus: DataTypes.STRING(255)
	}, {
		tableName: 'logging',
		timestamps: false
	})
	return Log
}
