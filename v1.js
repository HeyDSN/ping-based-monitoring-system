// Modules
const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
// Routes
const index = require('./api/routes/index')
const app = express()
// Express settings
app.set('views', path.join(__dirname, 'views'))
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
// Routes
app.use('/', index)
// Error handlers - Catch 404 and forward to error handler
app.use(function (req, res) {
	res.status(404).json({
		message: 'Path Not found',
		code: 404
	})
})
module.exports = app
