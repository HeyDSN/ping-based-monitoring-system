const express = require('express')
const router = express.Router()
const functionModels = require('../functions/model')
router.get('/', (_req, res) => {
	res.json({
		code: 200,
		message: 'Welcome to device monitoring',
		APIVersion: 'v1'
	})
})
router.get('/device-availability', (_req, res) => {	
	functionModels.getLog((_err, data) => {
		let idx = 0
		let status = [{
			deviceIP: 0
		}]
		try {
			data.forEach(obj => {
				if (status[ idx ].deviceIP === 0) {
					if (obj.deviceStatus === 'up') {
						status[ idx ] = { deviceIP: obj.deviceIP, deviceName: obj.deviceName, deviceUp: +obj.count, deviceDown: 0 }
					} else {
						status[ idx ] = { deviceIP: obj.deviceIP, deviceName: obj.deviceName, deviceUp: 0, deviceDown: +obj.count }
					}
				} else {
					if (status[ idx ].deviceIP === obj.deviceIP) {
						if (obj.deviceStatus === 'up') {
							status[ idx ].deviceUp = +obj.count
						} else {
							status[ idx ].deviceDown = +obj.count
						}
					} else {
						idx++
						if (obj.deviceStatus === 'up') {
							status[ idx ] = { deviceIP: obj.deviceIP, deviceName: obj.deviceName, deviceUp: +obj.count, deviceDown: 0
							}
						} else {
							status[ idx ] = { deviceIP: obj.deviceIP, deviceName: obj.deviceName, deviceUp: 0, deviceDown: +obj.count
							}
						}
					}
				}
			})
		} catch (e) {
			return res.status(500).json({
				APIVersion: 'v1',
				code: 500,
				data: {
					message: 'Internal Server Error',
				}
			})
		}
		let idy = 0
		status.forEach(obj => {
			let up = obj.deviceUp
			let down = obj.deviceDown
			let total = up + down
			let deviceAvailability = ((up/total) * 100).toFixed(2)
			status[ idy ].deviceAvailability = deviceAvailability + ' %'
			delete status[ idy ].deviceUp
			delete status[ idy ].deviceDown
			idy++
		})
		res.status(200).json({
			APIVersion: 'v1',
			code: 200,
			data: {
				message: 'OK',
				deviceStatus: status,
			}
		})
	})
})
module.exports = router
