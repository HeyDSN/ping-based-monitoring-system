const ping = require('ping')
const fs = require('fs')
const models = require('../../../api/models')
const Log = models.Log
const func = {
	check1995: async () => {
		let obj
		try {
			obj = JSON.parse(fs.readFileSync(process.cwd() + '/device-list.json', 'utf8'))
		} catch (e) {
			// Using example for testing
			obj = JSON.parse(fs.readFileSync(process.cwd() + '/device-list.example.json', 'utf8'))
		}
		const deviceList = obj.device
		let status
		let loop = 0
		return new Promise((resolve) => {
			for (let device of deviceList) {
				ping.sys.probe(device.deviceIP, (nyala) => {
					if (nyala === true) {
						status = 'up'
					} else {
						status = 'down'
					}
					Log.create({
						IP: device.deviceIP,
						deviceName: device.deviceName,
						deviceStatus: status
					}).then(()=>{
						loop++
						if (loop === deviceList.length - 1) resolve(true)
					})
				})
			}
		})
	}

}
module.exports = func
