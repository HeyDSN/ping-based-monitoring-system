const models = require('../models')
const Log = models.Log
const Sequelize = models.sequelize
const func = {
	getLog: (cb) => {
		return Log
			.findAll({
				attributes: [
					['IP', 'deviceIP'], 'deviceName', 'deviceStatus', [Sequelize.fn('COUNT', Sequelize.col('ID')), 'count']
				],
				group: ['IP', 'deviceName', 'deviceStatus'],
				order: [
					['deviceName', 'ASC'],
				],
				raw: true
			}).then((logs) => {
				return cb(null, logs)
			}).catch((err) => {
				return cb(null, err)
			})
	}
}

module.exports = func
