const CronJob = require('cron').CronJob
const monitor = require('./function')
// Run every 5 sec after server start
const jobReminder = new CronJob('*/5 * * * * *', async () => {
	monitor.check1995()
}, null, true, 'Asia/Jakarta')
jobReminder.start()
