require('dotenv').config()
process.env.NODE_ENV = 'test'
// Test module
const chai = require('chai')
chai.use(require('chai-http'))
const expect = require('chai').expect
// Main function
const cronFunction = require('../components/cronjob/monitor/function')
const api = require('../app.js')
// Test app
describe('Cron Test', function () {
	this.timeout(15000)
	it('Should ping device', async () => {
		this.timeout(15000)
		const res = await cronFunction.check1995()
		expect(res).be.ok
	})
})
describe('API Test', function () {
	it('should GET welcome message v1', function (done) {
		chai.request(api)
			.get('/')
			.then(function (res) {
				try {
					expect(res).to.have.status(200)
					expect(res).to.be.json
					expect(res.body).to.be.an('object')
					done()
				} catch (err) {
					done(err)
				}
			})
	})
	it('should GET 404', function (done) {
		chai.request(api)
			.get('/UNKNOWN')
			.then(function (res) {
				try {
					expect(res).to.have.status(404)
					done()
				} catch (err) {
					done(err)
				}
			})
	})
	it('should GET device availability result', function (done) {
		chai.request(api)
			.get('/device-availability')
			.then(function (res) {
				try {
					expect(res).to.have.status(200)
					expect(res).to.be.json
					expect(res.body).to.be.an('object')
					done()
				} catch (err) {
					done(err)
				}
			})
	})
})
