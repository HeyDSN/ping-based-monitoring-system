const config = require('../../config')
const Sequelize = require('sequelize')

const db = {
	sequelize: new Sequelize(config.sql.database, config.sql.username, config.sql.password, {
		dialect: config.sql.dialect,
		host: config.sql.host,
		port: config.sql.port,
		timezone: '+07:00',
		logging: false
	})
}
db.Sequelize = db.sequelize
db.Log = db.sequelize.import('./Log')

module.exports = db
