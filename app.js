/** Modules **/
const express = require('express')
const app = express()
/** Routes Versioning **/
const v1 = require('./v1')
/** App Routes Versioning **/
app.use('/v1', v1)
app.use('/', v1)
module.exports = app
